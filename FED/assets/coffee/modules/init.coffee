define ["jquery", "modules/util"], ($, Util) ->

	# EXAMPLE - UTIL - no requireIf needed feel free to delete
	# require ["modules/util"], (Util) ->
	# 	new Util
	$window = $(window)
	$body = $('body')
	$header = $('#header')

	# run functions if Feature Callout exists
	if $('#featureCallout').length && $body.hasClass 'template-home'
		# get #featureCallout height
		_featureHeight = $('#featureCallout').height() - $header.innerHeight()
		# run functions when window scrolls
		$window.on 'scroll', ->
			# check current scroll position
			_currScroll = $body.scrollTop()
			# if current scroll position is taller than the #featureCallout
			if _currScroll > _featureHeight
				$header.addClass 'solid-state'
			else
				$header.removeClass 'solid-state'