define ["jquery"], ($) ->
  $('#phone').keydown (event) ->
    # Allow only backspace and delete
    if event.keyCode == 46 or event.keyCode == 8
      # let it happen, don't do anything
    else
      # Ensure that it is a number and stop the keypress
      if event.keyCode < 48 or event.keyCode > 57
        event.preventDefault()
    return
  $('#zipcode').keydown (event) ->
    # Allow only backspace and delete
    if event.keyCode == 46 or event.keyCode == 8
      # let it happen, don't do anything
    else
      # Ensure that it is a number and stop the keypress
      if event.keyCode < 48 or event.keyCode > 57
        event.preventDefault()
    return
  