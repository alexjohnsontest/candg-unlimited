define ["jquery"], ($) ->
	return class Util
		constructor: (@options) ->
			@isMobile()
		isMobile: ->
			$html = $ "html"
			if /iPod|iPad|iPhone|Blackberry|Android|Windows Phone/i.test navigator.userAgent
				$html.addClass "is-mobile"