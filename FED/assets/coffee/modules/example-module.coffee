# EXAMPLE MODULE
define ["jquery"], ($) ->
  return class ExampleModule
    constructor: (@options) ->
      @delegateEvents()
    delegateEvents: ->
      $element = @options.$element
      _self = this
      # handle mobile links using coffeeScript single arrow
      $element.off().on "click", ".item-block", (event) ->
        _self.exampleMethod()
      # handle mobile links using coffeeScript single arrow
      $element.off().on "click", ".item-block", (event) =>
        @exampleMethod()
    exampleMethod: () ->
      return @options.exVar1 + @options.exVar2