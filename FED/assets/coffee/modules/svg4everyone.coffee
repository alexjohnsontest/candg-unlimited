((document, uses, CACHE) ->

  embed = (svg, g) ->
    if g
      viewBox = g.getAttribute('viewBox')
      fragment = document.createDocumentFragment()
      clone = g.cloneNode(true)
      if viewBox
        svg.setAttribute 'viewBox', viewBox
      while clone.childNodes.length
        fragment.appendChild clone.childNodes[0]
      svg.appendChild fragment
    return

  onload = ->
    xhr = this
    x = document.createElement('x')
    s = xhr.s
    x.innerHTML = xhr.responseText

    xhr.onload = ->
      s.splice(0).map (array) ->
        embed array[0], x.querySelector('#' + array[1].replace(/(\W)/g, '\\$1'))
        return
      return

    xhr.onload()
    return

  onframe = ->
    use = undefined
    while use = uses[0]
      svg = use.parentNode
      url = use.getAttribute('xlink:href').split('#')
      url_root = url[0]
      url_hash = url[1]
      svg.removeChild use
      if url_root.length
        xhr = CACHE[url_root] = CACHE[url_root] or new XMLHttpRequest
        if !xhr.s
          xhr.s = []
          xhr.open 'GET', url_root
          xhr.onload = onload
          xhr.send()
        xhr.s.push [
          svg
          url_hash
        ]
        if xhr.readyState == 4
          xhr.onload()
      else
        embed svg, document.getElementById(url_hash)
    return

  document.addEventListener 'DOMContentLoaded', onframe
  document.addEventListener 'DOMSubtreeModified', onframe
  onframe()
  return
) document, document.getElementsByTagName('use'), {}

