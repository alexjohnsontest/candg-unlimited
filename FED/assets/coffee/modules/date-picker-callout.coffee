define ["jquery" , "jqueryui"], ($) ->
  $(document).ready ->
    $('#checkin').datepicker
      onClose: ->
        $('#checkout').focus()
    $('#checkout').datepicker()

      