define(["jquery"], function($) {
  return $('.nav-toggle').on('click', function() {
    return this.classList.toggle('active');
  });
});
