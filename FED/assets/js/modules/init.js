define(["jquery", "modules/util"], function($, Util) {
  var $body, $header, $window, _featureHeight;
  $window = $(window);
  $body = $('body');
  $header = $('#header');
  if ($('#featureCallout').length && $body.hasClass('template-home')) {
    _featureHeight = $('#featureCallout').height() - $header.innerHeight();
    return $window.on('scroll', function() {
      var _currScroll;
      _currScroll = $body.scrollTop();
      if (_currScroll > _featureHeight) {
        return $header.addClass('solid-state');
      } else {
        return $header.removeClass('solid-state');
      }
    });
  }
});
