define(["jquery"], function($) {
  $('.feature-callout-renters__banner').on('click', function() {
    $('body').css({
      'position': 'fixed'
    });
    $('#hiddengallery').addClass('overlay');
    $('.overlay').css({
      'display': 'block'
    });
    return document.getElementById('header').style.display = 'none';
  });
  return $('#close').on('click', function() {
    $('body').css({
      'position': 'relative'
    });
    $('#hiddengallery').removeClass('overlay');
    $('.overlay').css({
      'display': 'none'
    });
    return document.getElementById('header').style.display = '';
  });
});
