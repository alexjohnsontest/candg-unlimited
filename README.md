FED Starter Kit
=========================
You can find the latest style guides at the [Project Starters](https://bitbucket.org/lightmakerusa/lm-project-starters).

---
### About

This **Kit** is a pre-made, ever-evolving, starting kit for Front-End Developers
---

## Setup (Frontend)

####How to start?

> 1. Read the CODE STYLE GUIDES
>> *	[Javascript Style Guide](https://github.com/airbnb/javascript?source=c)
>> *	[CoffeeScript Style Guide](https://github.com/LightmakerCanada/coffeescript-style-guide)
> 2. Download the Zip of this Repo
>> * https://your_login_id@bitbucket.org/lightmakerusa/lm-project-starter-kits-fed-starter-kit.git
> 3. Add both of these folders to your project.
> 4. Run Gulp on your project.
> 5. Code the rest of your project within the GUIDELINES.

####What you will need
> - Gulp - [How to install](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md#getting-started)
> - CoffeeScript - [How to install](http://coffeescript.org/)

---

## Using Gulp

> * Make sure to first install all the node modules by running "sudo npm install" on the root project directory
> * Simply run "gulp" after installation of the node modules is complete
> * If you want to just compile the assets and html simply run "grunt build"

---

## Notes for Front-End

> * Gulp spins up a node web server using express to view templates "http://localhost:4000/{templateName}"
> * There's two arguments you can use "?platform={platformValue}&direction={directionValue}"
> * "platform" accepts the following values ["desktop", "tablet", "mobile"] (Default value is desktop)
> * "direction" accepts the following value "rtl" (Default value is ltr)

---

## Post Installation Tasks
> * Update this readme.md to reflect the current client and project
> * Once you check out a branch (ie. Develop), click on GitFlow on the repository. This enables you to create features, hot fixes, etc.