define(["jquery", "flexslider"], function($) {
  return $('.feature-callout-renters__banner').on('click', function() {
    $('#carousel').flexslider({
      animation: 'slide',
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemWidth: 63,
      itemMargin: 5,
      asNavFor: '#slider'
    });
    return $('#slider').flexslider({
      animation: 'slide',
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      sync: '#carousel'
    });
  });
});
