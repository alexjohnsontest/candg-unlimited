define(["jquery", "jqueryval"], function($) {
  return $(document).ready(function() {
    $('#applicationForm').validate();
  });
});
