define([], function() {
  var requireIf;
  return requireIf = function(condition, moduleArray, exports) {
    if (exports == null) {
      exports = function() {};
    }
    if (condition) {
      return require(moduleArray, exports);
    }
  };
});
