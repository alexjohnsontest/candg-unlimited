define(["jquery", "flexslider"], function($) {
  return $('#featureCallout').click(function() {
    $('#carousel').flexslider({
      animation: 'slide',
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemWidth: 63,
      itemMargin: 5,
      keyboard: true,
      multipleKeyboard: true,
      startAt: 0,
      touch: true,
      asNavFor: '#slider'
    });
    return $('#slider').flexslider({
      animation: 'slide',
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      keyboard: true,
      multipleKeyboard: true,
      startAt: 0,
      touch: true,
      sync: '#carousel'
    });
  });
});
