define(["jquery"], function($) {
  var $lightbox, $map, $thumb;
  $lightbox = $('.lightbox');
  $thumb = $('.thumb img');
  $map = [];
  $(document).ready(function() {
    var $len;
    $thumb.each(function() {
      $map.push($(this));
    });
    $len = $map.length;
    if ($len > 0) {
      $lightbox.append($map[0].clone());
    } else {
      $lightbox.text('Aucune image.');
    }
  });
  $thumb.on('click', function(e) {
    var $index, $item;
    $item = $(this).parent();
    $index = $('li').index($item);
    console.log($index);
    e.preventDefault();
    $lightbox.html($map[$index].clone());
  });
  return $('.feature-callout-renters__banner').on('click', function() {
    $('body').toggleClass('overlay');
    return $('.overlay').innerHTML = "<div id='content'>" + "<div id='viewer'>" + "<div class='lightbox'>" + "</div>" + "<div class='sidebar'>" + "<ul>" + "<li class='thumb'>" + "<img src='http://images6.alphacoders.com/303/303836.jpg' width='100px'>" + "</li>" + "<li class='thumb'>" + "<img src='http://hdwallpaperd.com/wp-content/uploads/city_of_love-wallpaper-5120x3200.jpg'>" + "</li>" + "<li class='thumb'>" + "<img src='http://hdwallpaperd.com/wp-content/uploads/new-wallpaper-16.jpg'>" + "</li>" + "</ul>" + "</div>" + "</div>" + "</div>";
  });
});
