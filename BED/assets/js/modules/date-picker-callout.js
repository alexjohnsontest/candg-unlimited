define(["jquery", "jqueryui"], function($) {
  return $(document).ready(function() {
    $('#checkin').datepicker({
      onClose: function() {
        return $('#checkout').focus();
      }
    });
    return $('#checkout').datepicker();
  });
});
