requirejs.config({
  baseUrl: "/assets/js",
  waitSeconds: 1000,
  paths: {
    jquery: "https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min",
    jqueryui: "vendor/jquery-ui.min",
    jqueryval: "http://cdn.wideskyhosting.com/js/jquery.validate",
    flexslider: "vendor/jquery.flexslider-min",
    featherlight: "../js/vendor/featherlight.min"
  },
  shim: {
    "jquery.mobile.events": {
      deps: ["jquery"]
    },
    "jqueryui": {
      deps: ["jquery"]
    },
    "jqueryval": {
      deps: ["jquery"]
    },
    "flexslider": {
      deps: ["jquery"]
    },
    "featherlight": {
      deps: ["jquery"]
    }
  }
});

require(["modules/init"]);

require(["modules/reveal"]);

require(["modules/date-picker-callout"]);

require(["modules/onlynumbers"]);

require(["modules/validate"]);

require(["modules/selection"]);

require(["modules/gallerypopup"]);

require(["modules/hamburger"]);

require(["modules/popup"]);

require(["modules/featherlight"]);
