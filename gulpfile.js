// FED Paths
var BASE_FED = "FED";
var ASSETS_FED = BASE_FED + "/assets";
var JS_FED = ASSETS_FED + "/js";
var JS_VENDOR_FED = JS_FED + "/vendor";
var COFFEE_FED = ASSETS_FED + "/coffee";
var SASS_FED = ASSETS_FED + "/sass";
var CSS_FED = ASSETS_FED + "/css";
var IMAGES_FED = "/" + ASSETS_FED + "/images";
var IMAGES_FED_COPY = ASSETS_FED + "/images";
var FONTS_FED = ASSETS_FED + "/fonts";
var ICONS_FED = ASSETS_FED + "/icons";
var JADE_FED = BASE_FED + "/jade";

// BED Paths
var BASE_BED = "BED";
var ASSETS_BED = BASE_BED + "/assets";
var JS_BED = ASSETS_BED + "/js";
var JS_VENDOR_BED = JS_BED + "/vendor";
var CSS_BED = ASSETS_BED + "/css";
var IMAGES_BED = ASSETS_BED + "/images";
var FONTS_BED = ASSETS_BED + "/fonts";
var ICONS_BED = ASSETS_BED + "/icons";
var HTML_BED = BASE_BED + "/html";

// FED File Paths
var COFFEE_FILES = COFFEE_FED + "/**/*.coffee";
var SASS_FILES = SASS_FED + "/**/*.scss";
var IMAGES_FILES = IMAGES_FED + "/**/*.*";
var IMAGES_FILES_COPY = IMAGES_FED_COPY + "/**/*.*";
var FONTS_FILES = FONTS_FED + "/**/*.*";
var ICONS_FILES = ICONS_FED + "/**/*.svg";
var JADE_FILES = JADE_FED + "/**/*.jade";
var CSS_FILES = CSS_FED + "/**/*.css";
var JS_FILES = JS_FED + "/**/*.*";
var JS_VENDOR_FILES = JS_VENDOR_FED + "/**/*.*";
var LIVERELOAD_FILES = [CSS_FILES, JS_FILES, JADE_FILES];

// If you're having any live reload issues feel free to modify this constant
var LIVERELOAD_DELAY = 0;

// Build Tasks
var buildTasks = ['sass', 'coffee', 'misc'];

// Include gulp
var gulp = require('gulp');
var svgo = require('gulp-svgo');
var svgSprite = require('gulp-svg-sprite');

// Include Our Plug-ins
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var coffee = require('gulp-coffee');
var minifyCSS = require('gulp-minify-css');
var gutil = require('gulp-util');
var imagemin = require('gulp-imagemin');
var gulpJade = require('gulp-jade');
var uglify = require('gulp-uglify');
var bless = require('gulp-bless');
var pngcrush = require('imagemin-pngcrush');
var jade = require('jade');
var prettify = require('gulp-prettify');
var bourbon = require('node-bourbon').includePaths;
var plumber = require('gulp-plumber');

// Live Reload
var EXPRESS_PORT = 4000;
var EXPRESS_ROOT = BASE_FED;
var LIVERELOAD_PORT = 35729;
var lr;
var startExpress = function() {
  var express = require('express');
  var app = express();
  var favicon = require('serve-favicon');
  app.use(favicon(__dirname + '/' + IMAGES_FED + "/global/favicon.ico"));
  app.use(require('connect-livereload')());
  app.use(express.static(EXPRESS_ROOT))
  app.set('view engine', 'jade');
  app.set('views', __dirname + "/" + JADE_FED + "/templates/");

  app.locals.platform = "desktop";
  app.locals.htmlBuildFlag = false;

  app.get('/', function(req, res) {
    if ("platform" in req.query) {
      app.locals.platform = req.query.platform;
    }
    res.render("index");
  });
  app.get('/:fileName', function(req, res) {
    if ("platform" in req.query) {
      app.locals.platform = req.query.platform;
    }
    res.render(req.params.fileName);
  });

  app.listen(EXPRESS_PORT);
};
var startLivereload = function() {
  lr = require('tiny-lr')();
  lr.listen(LIVERELOAD_PORT);
};
var notifyLivereload = function(event) {
  var fileName = require('path').relative(EXPRESS_ROOT, event.path);
  setTimeout(function(){
    lr.changed({
      body: {
        files: [fileName]
      }
    });
  }, LIVERELOAD_DELAY);
};

// Sass Task
gulp.task('sass', function() {
  gulp.src(SASS_FILES)
    .pipe(sass({
      errLogToConsole: true,
      outputStyle: 'nested',
      sourceComments: false,
      includePaths: bourbon
    }))
    .pipe(gulp.dest(CSS_FED))
    //.pipe(minifyCSS({advanced:false}))
    // in order for bless to work correctly it needs to strip out comments before it parses the CSS
    // .pipe(bless({
    //   cacheBuster: true,
    //   cleanup: true,
    //   compress: false
    // }))
    .pipe(gulp.dest(CSS_BED));
});

// Misc Tasks
gulp.task('misc', function(){
    // move icons to deploy and bed
    gulp.src(ICONS_FILES)
      .pipe(gulp.dest(ICONS_BED));
    // move fonts to deploy and bed
    gulp.src(FONTS_FILES)
      .pipe(gulp.dest(FONTS_BED));
    // move images to deploy and bed
    gulp.src(IMAGES_FILES_COPY)
      .pipe(gulp.dest(IMAGES_BED));
})

// Coffee Task
gulp.task('coffee', function() {
  gulp.src(COFFEE_FILES)
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(gulp.dest(JS_FED));
  // uglify and move vendor to deploy folder

  gulp.src(JS_FILES)
    .pipe(gulp.dest(JS_BED));
});

// Minify Images
gulp.task('images', function() {
  gulp.src(IMAGES_FILES)
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngcrush()]
    }));
});

// Sprite
gulp.task('sprite', function () {

  return gulp.src("FED/assets/svgs/**/*.svg")
    .pipe(svgo())
    .pipe(svgSprite({
    "log": "debug",
    "shape": {
        "dimension": {
            "maxHeight": 68
        }
    },
    "mode": {
        "symbol": {
          "dest": "./images",
          "sprite": "sprite.svg",
          "example": {
            "dest": "../svgs/sprite.html"
          }
        }
    }})).pipe(gulp.dest("FED/assets"));

});


// Call Render Tasks
// this is adding the JADE build tasks for each platform to this build
//generateTemplate();

// Templates Watch
gulp.task('watch', function() {
   startExpress();
   startLivereload();
   gulp.watch(COFFEE_FILES, ['coffee']);
   gulp.watch(SASS_FILES, ['sass']);
   gulp.watch(LIVERELOAD_FILES, notifyLivereload);
});

// Tasks
gulp.task('build', buildTasks);
// Templates Render Function
gulp.task('html', function() {
  var _destination = HTML_BED + "/";
  var _locals = {
                  htmlBuildFlag: true
                };
  gulp.src(JADE_FILES)
    .pipe(gulpJade({
      pretty: true,
      locals: _locals
    }))
    .pipe(prettify({indent_size: 2}))
    .pipe(gulp.dest(_destination));
});
gulp.task('dev', ['build', 'html', 'images']);
gulp.task('default', ['build', 'watch']);
